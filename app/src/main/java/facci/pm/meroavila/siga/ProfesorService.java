package facci.pm.meroavila.siga;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ProfesorService {

    //le decimos que esta referencia de la url sera la llave donde buscara todo los profesores
    @GET("teacher/")
    Call<List<Profesor>> getAllProfesor();

    //le decimos que esta referencia de la url sera la llave donde buscara todo los profesores
    //esto es para decirle al servidor que querie hacer eso en la api
    @POST("teacher/")
    Call<Profesor> guardarProfesor(@Body Profesor profesor);
}
