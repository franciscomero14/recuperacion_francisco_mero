package facci.pm.meroavila.siga.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import facci.pm.meroavila.siga.ApiProfesor;
import facci.pm.meroavila.siga.Profesor;
import facci.pm.meroavila.siga.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    //instanciamos los view
    EditText cedu,ciudad,biografia,nombre,apellido;
    Button post,get;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Referenciando los view
        cedu = findViewById(R.id.edt_cedula_profesor);
        ciudad = findViewById(R.id.edt_ciudad_profesor);
        biografia = findViewById(R.id.edt_biografia_profesor);
        nombre = findViewById(R.id.edt_nombre_profesor);
        apellido = findViewById(R.id.edt_apellido_profesor);

        //referenciando los botones
        post = findViewById(R.id.btn_post_profesor);
        get = findViewById(R.id.btn_get_profesor);

        //asignandole un evento al boton de un click
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarProfesor(crearRequest());//llamando a las clases de guardarProfesor y crearRequest
            }
        });

        //asignandole un evento al boton de un click
        get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,Indiviual.class));//llamando a la activity
            }
        });

    }

    //
    public Profesor crearRequest(){

        Profesor profesor = new Profesor();//creando un objeto, para enviarle los paramatros
        profesor.setCedula(cedu.getText().toString());
        profesor.setCiudad(ciudad.getText().toString());
        profesor.setBiografia(biografia.getText().toString());
        profesor.setNombre(nombre.getText().toString());
        profesor.setApellido(apellido.getText().toString());

        return profesor;
    }

    public void guardarProfesor(Profesor profesor){

        Call<Profesor> profesorCall = ApiProfesor.getProfesorService().guardarProfesor(profesor); //es una clase que devuelve Retrofit conteniendo el modelo de respuesta.
        profesorCall.enqueue(new Callback<Profesor>() {
            @Override
            public void onResponse(Call<Profesor> call, Response<Profesor> response) {//una ves creado el endpoint lo enviamos a la post de la rest
                if (response.isSuccessful()){
                    Toast.makeText(MainActivity.this,"¡Guardado con exito!",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(MainActivity.this,"¡solicitud fallida!",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Profesor> call, Throwable t) {
                Toast.makeText(MainActivity.this,"¡solicitud fallida!"+t.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
            }
        });

    }
}