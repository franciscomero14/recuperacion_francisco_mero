package facci.pm.meroavila.siga;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProfesorAdapter extends RecyclerView.Adapter<ProfesorAdapter.ProfesorViewHolder> {

    private List<Profesor> profesorResponseList; //llamamos el objeto en una lista
    private Context context;//referenciamos el contexto

    public ProfesorAdapter() { //creamos un constructor para el adaptador
    }

    public void setData(List<Profesor> profesorList){//seteamos los datos del objeto creado
            this.profesorResponseList = profesorList;
            notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProfesorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ProfesorAdapter.ProfesorViewHolder(LayoutInflater.from(context).inflate(R.layout.item_docente,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProfesorViewHolder holder, int position) {
        Profesor profesor = profesorResponseList.get(position);

        String nomb = profesor.getNombre();
        String apelli = profesor.getApellido();
        String ciud = profesor.getCiudad();
        String cedu = profesor.getCedula();

        holder.nombre.setText(nomb);
        holder.apellido.setText(apelli);
        holder.ciudad.setText(ciud);
        holder.cedula.setText(cedu);
    }

    @Override
    public int getItemCount() {
        return profesorResponseList.size();
    }

    public class ProfesorViewHolder extends RecyclerView.ViewHolder {

        TextView nombre;
        TextView apellido;
        TextView ciudad;
        TextView cedula;

        public ProfesorViewHolder(@NonNull View itemView) {
            super(itemView);

            nombre = itemView.findViewById(R.id.item_nombre);
            apellido = itemView.findViewById(R.id.item_apellido);
            ciudad = itemView.findViewById(R.id.item_ciudad);
            cedula = itemView.findViewById(R.id.item_cedula);

        }
    }
}
