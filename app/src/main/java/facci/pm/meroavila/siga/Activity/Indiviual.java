package facci.pm.meroavila.siga.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.List;

import facci.pm.meroavila.siga.ApiProfesor;
import facci.pm.meroavila.siga.Profesor;
import facci.pm.meroavila.siga.ProfesorAdapter;
import facci.pm.meroavila.siga.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Indiviual extends AppCompatActivity {

    //instanciamos los view
    ImageView atras;

    RecyclerView recyclerView;

    ProfesorAdapter profesorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indiviual);

        //Referenciando los view
        atras = findViewById(R.id.btn_atras);

        recyclerView = findViewById(R.id.RecyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));

        profesorAdapter = new ProfesorAdapter();
        getAllProfesor();

        //asignandole un evento a la imageview de un click
        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Indiviual.this,MainActivity.class));
                finish();
            }
        });
    }

    //obtenemos todas las respuesta de los profesores
    public void getAllProfesor(){
        Call<List<Profesor>> profesorList = ApiProfesor.getProfesorService().getAllProfesor();

        profesorList.enqueue(new Callback<List<Profesor>>() {
            @Override
            public void onResponse(Call<List<Profesor>> call, Response<List<Profesor>> response) {
                if(response.isSuccessful()){
                    List<Profesor> profesorListResponse = response.body();//obetenmos todos los profesores y se le asigna al recyclerview
                    profesorAdapter.setData(profesorListResponse);
                    recyclerView.setAdapter(profesorAdapter);

                }
            }

            @Override
            public void onFailure(Call<List<Profesor>> call, Throwable t) {
                Log.e("Fallido",t.getLocalizedMessage());
            }
        });
    }
}